package controllers

import (
	"github.com/astaxie/beego"
	"io/ioutil"
	"os"
	"path"
	"strings"
	"path/filepath"
	"strconv"
)

type MovieTypes struct {
	Id int `json:"id"`
	Type string `json:"type"`
	Movies []Movies `json:"movies"`
	Num int `json:"num"`
}

type Movies struct {
	Image string `json:"image"`
	Name string `json:"name"`
	Video string `json:"video"`
	Id int `json:"id"`
}

//定义全局变量
var AllMovies []MovieTypes

type VideolistController struct {
	beego.Controller
}

func (c *VideolistController) VideoTypes() {
	if len(AllMovies)==0 {
		Getfile()
	}
	json_data:=[]string{}
	for _, types := range AllMovies {
		json_data=append(json_data,types.Type)
	}
	c.Data["json"]=json_data
	c.ServeJSON()
}

func (c *VideolistController) Videolist() {
	//alert:=Grafana{}
	//log.SetPrefix("[DEBUG 1]")
	//log.Println(string(c.Ctx.Input.RequestBody))
	//json.Unmarshal(c.Ctx.Input.RequestBody, &alert)
	PageType,_:=strconv.Atoi(c.Input().Get("type"))

	if len(AllMovies)==0 {
		Getfile()
	}
	json_data:=MovieTypes{}
	json_data.Type=AllMovies[PageType].Type
	json_data.Num=len(AllMovies[PageType].Movies)
	json_data.Movies=AllMovies[PageType].Movies
	json_data.Id=AllMovies[PageType].Id
	c.Data["json"]=json_data
	c.ServeJSON()
}

func (c *VideolistController) Videoreload() {
	oldm:=0
	for _, m := range AllMovies {
		oldm+=len(m.Movies)
	}
	AllMovies=nil
	Getfile()
	newm:=0
	for _, m := range AllMovies {
		newm+=len(m.Movies)
	}
	c.Data["json"]="reload ok ! 原数量:"+strconv.Itoa(oldm)+"  更新后数量:"+strconv.Itoa(newm)
	c.ServeJSON()
}

func (c *VideolistController) Videodel() {
	vname:=c.Input().Get("name")
	delok:=""
	//删除视频和图片
	delerr:=os.Remove(vname)
	if delerr!=nil {
		delok=delok+"del video error!"
		}
	delerr=os.Remove(vname + ".jpg")
	if delerr!=nil {
		delok=delok+"del img error!"
		}
	AllMovies=nil
	if delok=="" {
		delok="del ok !"
	}
	c.Data["json"]=delok
	c.ServeJSON()
}

//获取指定目录下的所有文件和目录
func ListDir(dirPth string) (files []string,files1 []string, err error) {
	//fmt.Println(dirPth)
	dir, err := ioutil.ReadDir(dirPth)
	if err != nil {
		return nil,nil, err
	}
	PthSep := string(os.PathSeparator)
	//    suffix = strings.ToUpper(suffix) //忽略后缀匹配的大小写
	for _, fi := range dir {
		if fi.IsDir() { // 忽略目录
			files1 = append(files1, dirPth+PthSep+fi.Name())
			ListDir(dirPth + PthSep + fi.Name())
		}else{
			//fmt.Println("s")
			files = append(files, dirPth+PthSep+fi.Name())
		}
	}
	return files,files1, nil
}
func Getfile() {
	Videos:=MovieTypes{}
	Video:=Movies{}
	FileDirs:=beego.AppConfig.String("dir")
	FileDir:=strings.Split(FileDirs,",")
	for _, Dir := range FileDir {
		files,files1, _ := ListDir(Dir)
		for _, table := range files1 {
			temp,_,_:=ListDir(table)
			for _,temp1 :=range temp{
				files = append(files, temp1)
			}
		}

		for key, table2 := range files1 {
			Videos.Id=key
			Videos.Type=table2
			AllMovies=append(AllMovies,Videos)
		}
		for _, table1 := range files {
			//排除后缀为jpg的文件
			if path.Ext(table1)!=".jpg" && path.Ext(table1)!=".avi" {
				for i, types := range AllMovies {
					if types.Type==filepath.Dir(table1) {
						Video.Video=table1[3:len(table1)]
						jpg:=table1+".jpg"
						Video.Image=jpg[3:len(jpg)]
						Video.Name=table1
						AllMovies[i].Movies=append(AllMovies[i].Movies,Video)
					}

				}

			}
		}
	}

}
