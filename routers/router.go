package routers

import (
	"github.com/astaxie/beego/plugins/cors"
	"myvideo/controllers"
	"github.com/astaxie/beego"
)

func init() {
	beego.InsertFilter("*", beego.BeforeRouter, cors.Allow(&cors.Options{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowHeaders:     []string{"Origin", "Authorization", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length", "Access-Control-Allow-Origin", "Access-Control-Allow-Headers", "Content-Type"},
		AllowCredentials: true,
	}))

    beego.Router("/", &controllers.MainController{})
	beego.Router("/videotype", &controllers.VideolistController{},"get,post:VideoTypes")
	beego.Router("/videolist", &controllers.VideolistController{},"get,post:Videolist")
	beego.Router("/videoreload", &controllers.VideolistController{},"get,post:Videoreload")
	beego.Router("/videodel", &controllers.VideolistController{},"get,post:Videodel")
}
